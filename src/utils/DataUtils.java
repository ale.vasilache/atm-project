package utils;

import User.BankAccount;
import User.BankCustomer;
import User.card.Card;
import User.card.CreditCard;

public class DataUtils {

    public static BankCustomer getCustomer() {
        BankCustomer customer = new BankCustomer(01, "28911050000001", "Alexandra", "Vasilache");
        BankAccount account = new BankAccount("RO0122354665CNTRL2556", "RON");
        account.updateBalance(150);
        Card card = new Card("5826412554478951", "3838");
        account.setCard(card);
        customer.setAccount(account);
        return customer;
    }

}
