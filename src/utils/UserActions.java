package utils;

import Output.ATMDispatcher;
import UI.Display;
import User.BankCustomer;

public class UserActions {

    public static void withdraw(BankCustomer someone) {
        int amount = Display.askUserForAmount();
        ATMDispatcher.withdraw(someone.getBankAccount(), amount);
        Display.showCustomerBalanceSheet(someone.getBankAccount());
    }

    public static void changePin(BankCustomer someone) {
        String initialPin = Display.askForPin();
        boolean isValid = someone.validatePin(initialPin);
        if (!isValid) {
            Display.displayInvalidPinMsg();
            return;
        }
        String changedPin = Display.askForNewPin();
        String confirmedPin = Display.confirmNewPin();

        boolean pinMatches = someone.validateChangedPin(changedPin, confirmedPin);
        if (!pinMatches) {
            Display.displayInvalidPinMsg();
            return;
        }
        someone.updatePin(changedPin);
        System.out.println("Pin Correct. Update pin: " + changedPin + " and try again");
    }

    public static void depositCash(BankCustomer someone) {
        int amount = Display.askUserForAmount();
        someone.depositCash(amount);
        Display.showCustomerBalanceSheet(someone.getBankAccount());
    }

    public static void deactivateAccount(BankCustomer someone) {
        someone.deactivateAccount();
        Display.informUserWithDeactivation();
    }

}
