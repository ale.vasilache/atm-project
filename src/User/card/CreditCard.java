package User.card;

public class CreditCard extends Card {

    private double creditLimit;

    public CreditCard(String cardNumber, String pin) {
        super(cardNumber, pin);
    }

    public void updateCreditLimit(double expense) {
        creditLimit = creditLimit - expense;

    }

}
