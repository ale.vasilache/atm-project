package User;

import User.card.Card;

public class BankAccount {
    private final String accountNumber;

    private final String currency;

    private double balance = 0;
    private Card card;

    public BankAccount(String accountNumber, String currency) {
        this.accountNumber = accountNumber;
        this.currency = currency;
    }

    public void setCard(Card card) {
        this.card = card;
    }
    public Card getCard() {
        return card;
    }
    public void updateBalance(double balance) {
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public String getCurrency(){
        return currency;
    }
    public String getAccountNumber(){
        return  accountNumber;
    }

}
