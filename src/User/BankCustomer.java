package User;

import Input.UserInput;

public class BankCustomer extends Person {
    private final int id;
    private BankAccount bankAccount;

    private boolean isActive;

    public BankCustomer(int bID, String cnp, String firstName, String lastName) {
        super(cnp, firstName, lastName);
        this.id = bID;
        this.isActive = true;
    }

    public void deactivateAccount() {
        this.isActive = false;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }


    public boolean validatePin(String input) {
        return bankAccount.getCard().verifyPin(input);
    }


    public void withdrawCash(String input) {
        System.out.println("Person " + getFirstName() + " " + getLastName() + " withdraw " + input + " RON");

    }

    public String selectOptionMenu() {
        String option = UserInput.readFromKeyboard();
        System.out.println("Person " + getFirstName() + " selected " + option);
        return option;
    }

    public boolean validateChangedPin(String changedPin, String confirmedPin) {
        return changedPin.equals(confirmedPin);
    }

    public void updatePin(String changedPin) {
        bankAccount.getCard().updatePin(changedPin);
    }

    @Override
    public String getFullName() {
        return getFirstName() + " " + getLastName();
    }

    public void depositCash(int amount) {
        double balance = bankAccount.getBalance();
        bankAccount.updateBalance(balance + amount);
    }
}
