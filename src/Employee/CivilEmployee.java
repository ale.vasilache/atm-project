package Employee;

public class CivilEmployee extends Employee {

    private final String signature;

    public CivilEmployee(String cnp, String firstName, String lastName, String title, String signature) {
        super(cnp, firstName, lastName, title);
        this.signature = signature;
    }

    @Override
    public String getFullName() {
        return signature + "\n" + super.getFullName();
    }
}
