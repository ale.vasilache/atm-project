import Employee.Employee;
import Employee.CivilEmployee;
import UI.Display;
import User.BankCustomer;
import utils.DataUtils;
import utils.UserActions;

import static Input.UserInput.authenticate;

public class AtmApp {

    public static void main(String[] args) {
        BankCustomer someone = DataUtils.getCustomer();

        Display.showWelcomeMsg();

        boolean passThrough = authenticate(someone);
        if (!passThrough) {
            Display.lockUserFor24h();
            return;
        }

        Display.showMenu();

        String option = someone.selectOptionMenu();
        switch (option) {
            case "0" -> Display.repairAtm(initCivilEmployee());
            case "1" -> UserActions.withdraw(someone);
            case "2" -> Display.showCustomerBalanceSheet(someone.getBankAccount());
            case "3" -> UserActions.changePin(someone);
            case "4" -> Display.activateCardMessage(someone);
            case "5" -> UserActions.depositCash(someone);
            case "6" -> UserActions.deactivateAccount(someone);
            case "7" -> Display.showIban(someone);
            case "8" -> Display.showCardDetails(someone.getBankAccount().getCard());

        }
    }

    private static Employee initEmployee() {
        return new Employee("28911058899585", "Dorel", "Dorica", "Mr.");
    }

    private static CivilEmployee initCivilEmployee(){
        return new CivilEmployee("28911055558895", "Matei", "Dima", "Mr.", "CFO");
    }

}