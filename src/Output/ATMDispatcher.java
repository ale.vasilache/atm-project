package Output;

import UI.Display;
import User.BankAccount;

public class ATMDispatcher {
    public static void withdraw(BankAccount account, int amount) {
        double balance = account.getBalance();
        Display ui = new Display();
        if (balance < amount) {
            Display.showNotEnoughMoneyMsg();
            return;
        }
        account.updateBalance(balance - amount);
    }
}

