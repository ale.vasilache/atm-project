package UI;

import Employee.Employee;
import Input.UserInput;
import User.BankAccount;
import User.BankCustomer;
import User.card.Card;

public class Display {
    public static final String ATM_NAME = "Free Cash ATM";

    public static final String APP_MENU =
            """
                    Select Option
                    1. Withdraw Cash
                    2. Check Balance
                    3. Change Pin
                    4. Activate Card   
                    5. Deposit cash   
                    6. Deactivate Bank Account             
                    7. Show IBAN
                    8. Show Card details
                    """;

    public static void showWelcomeMsg() {
        System.out.println("Welcome to " + ATM_NAME);
    }

    private static String readFromKeyboard() {
        return UserInput.readFromKeyboard();
    }

    public static String askForPin() {
        System.out.println("Please enter your pin number: ");
        return readFromKeyboard();
    }

    public static String askForNewPin() {
        System.out.println("Please enter your new pin number: ");
        return readFromKeyboard();
    }

    public static String confirmNewPin() {
        System.out.println("Please confirm your new pin number: ");
        return readFromKeyboard();
    }

    public static void newPinConfirmation() {
        System.out.println("Pin Number was changed");
    }

    public static void lockUserFor24h() {
        System.out.println("Incorrect Pin, Card locked for 24hours");
    }

    public static void displayInvalidPinMsg() {
        System.out.println("Pin incorrect, please try again.");
    }

    public static void showMenu() {
        System.out.println(APP_MENU);
    }

    public static void showCustomerBalanceSheet(BankAccount bankAccount) {
        System.out.println("Your balance is: " + bankAccount.getBalance() + " " + bankAccount.getCurrency());
    }

    public static void showNotEnoughMoneyMsg() {
        System.out.println("Your bank account does not have enough cash");
    }

    public static int askUserForAmount() {
        System.out.println("Type in amount you want: ");
        return UserInput.readIntFromKeyboard();
    }

    public static void activateCardMessage(BankCustomer someone) {
        System.out.printf("Thank you %s for activating the card", someone.getFullName());
    }

    public static void informUserWithDeactivation() {
        System.out.println("Thank you for being a valuable customer. Hope you come back");
    }

    public static void showIban(BankCustomer someone) {
        System.out.println("Iban: " + someone.getBankAccount().getAccountNumber());

    }

    public static void showCardDetails(Card card) {
        System.out.println("Card details: " + card.getCardNumber());

    }

    public static void repairAtm( Employee Employee) {
        System.out.println("Employee " +  Employee.getFullName() + " is repairing the ATM");

    }
}